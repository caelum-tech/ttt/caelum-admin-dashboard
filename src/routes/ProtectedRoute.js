import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

export const ProtectedRoute = ({ component: Component, authUser, ...rest }) => {
  return (
    <Route
      { ...rest }
      render={ props =>
        authUser
          ? <Component {...props} />
          : <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
      }
    />
  )
}

ProtectedRoute.propTypes = {
  component: PropTypes.any,
  authUser: PropTypes.any
}
