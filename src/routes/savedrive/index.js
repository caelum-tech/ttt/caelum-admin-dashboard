import React, { Component, Fragment } from 'react'
import {
  Row,
  Card,
  CardBody,
  CardTitle,
  ListGroup,
  ListGroupItem
} from 'reactstrap'

import { Colxx } from 'Components/CustomBootstrap'
import { LineShadow } from 'Components/Charts'
import { lineChartConfig } from 'Constants/chartConfig'

export default class SaveDrive extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: {
        stats: {},
        blocks: [{ author: '' }, { author: '' }, { author: '' }, { author: '' }],
        transactions: ['', '', '']
      },
      transactionsArray: [12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12],
      timer: false
    }
  }

  async UnsafecomponentWillMount () {
    if (this.state.timer === false) {
      setInterval(() => {
        const url = 'http://localhost:8000/stats.json'

        fetch(url)
          .then(response => response.json())
          .then(block => {
            console.log('RESPONSE BLOCK: ', block)
            const transactionsArray = block.transactions_array.map(value => parseInt(value))
            console.log('TRANSACTIONS ARRAY: ', transactionsArray)
            this.setState({ data: block, transactionsArray })
          })
      }, 3000)
      this.setState({ timer: true })
    }
  }

  render () {
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12" className="mb-4">
            <CardTitle className="mb-4">Blockchain Dashboard : Stats and Transactions</CardTitle>
            <Row className="icon-cards-row mb-2">
              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <p className="card-text font-weight-semibold mb-0">Block Author</p>
                    <p className="lead text-center">{this.state.data.blocks[0].author}</p>
                    <p className="card-text font-weight-semibold mb-0">Transactions</p>
                    <p className="lead text-center">{parseInt(this.state.data.blocks[0].transactions)}</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <p className="card-text font-weight-semibold mb-0">Block Author</p>
                    <p className="lead text-center">{this.state.data.blocks[1].author}</p>
                    <p className="card-text font-weight-semibold mb-0">Transactions</p>
                    <p className="lead text-center">{parseInt(this.state.data.blocks[1].transactions)}</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <p className="card-text font-weight-semibold mb-0">Block Author</p>
                    <p className="lead text-center">{this.state.data.blocks[2].author}</p>
                    <p className="card-text font-weight-semibold mb-0">Transactions</p>
                    <p className="lead text-center">{parseInt(this.state.data.blocks[2].transactions)}</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <p className="card-text font-weight-semibold mb-0">Block Author</p>
                    <p className="lead text-center">{this.state.data.blocks[3].author}</p>
                    <p className="card-text font-weight-semibold mb-0">Transactions</p>
                    <p className="lead text-center">{parseInt(this.state.data.blocks[3].transactions)}</p>
                  </CardBody>
                </Card>
              </Colxx>
            </Row>
          </Colxx>
        </Row>

        <Row>
          <Colxx xxs="12" className="mb-4">
            <CardTitle className="mb-4">Network Sats</CardTitle>
            <Row className="icon-cards-row mb-2">
              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Target"/>
                    <p className="card-text font-weight-semibold mb-0">Block Author</p>
                    <p className="lead text-center">{parseInt(this.state.data.stats.block_height)}</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Receipt-4"/>
                    <p className="card-text font-weight-semibold mb-0">Transactions per Block</p>
                    <p className="lead text-center">{parseInt(this.state.data.stats.tx_per_block)}</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Stopwatch"/>
                    <p className="card-text font-weight-semibold mb-0">Average Block Time</p>
                    <p className="lead text-center">{parseInt(this.state.data.stats.avg_block_time)}&apos;s</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="3">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Affiliate"/>
                    <p className="card-text font-weight-semibold mb-0">Validating Peers</p>
                    <p className="lead text-center">{parseInt(this.state.data.stats.validators)}</p>
                  </CardBody>
                </Card>
              </Colxx>
            </Row>
          </Colxx>
        </Row>
        {
          <Row>
            <Colxx xxs="12" className="mb-4">
              <CardTitle className="mb-4">Transaction History </CardTitle>
              <Row>
                <CardBody>
                  <div className="dashboard-line-chart">
                    <LineShadow {...lineChartConfig(this.state.transactionsArray)} />
                  </div>
                </CardBody>
              </Row>
            </Colxx>
          </Row>
        }
        <Row className="icon-cards-row mb-2">
          <Colxx xxs="12" sm="12" md="12" lg="12">
            <CardTitle className="mb-4">Blockchain Transactions</CardTitle>
            <ListGroup>
              {
                this.state.data.transactions.map((item, key) =>
                  <ListGroupItem key={key}>
                    <Row>
                      <Colxx xss="4">
                        <strong>Transaction : </strong>{item}
                      </Colxx>
                      <Colxx xss="4">
                        <a href={'https://goerli.etherscan.io/tx/' + item} target="_blank"
                          rel="noopener noreferrer">{item.txHash}</a>
                      </Colxx>
                    </Row>
                  </ListGroupItem>
                )}
            </ListGroup>
          </Colxx>
        </Row>
      </Fragment>
    )
  }
}
