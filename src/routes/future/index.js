import React, { Component, Fragment } from 'react'
import { Row, Card, CardBody, Button, CardTitle, ListGroup, ListGroupItem } from 'reactstrap'

import { Colxx } from 'Components/CustomBootstrap'

export default class Future extends Component {
  constructor (props) {
    super(props)
    this.state = {
      status: {
        loaded: false,
        deployed: 0,
        granted: 0,
        revoked: 0,
        campaigns: 0,
        users: 0,
        tx: [],
        claims: []
      }
    }

    this.timer = setInterval(() => this.load(), 5000)
  }

  componentDidMount () {
    this.load()
  }

  loadStatus () {
    return new Promise((resolve) => {
      // Load status.
      fetch('http://vodafone.caelumlabs.com:3000/status/4')
        .then(response => response.json())
        .then(function (data) {
          resolve(data)
        })
    })
  }

  loadClaims () {
    return new Promise((resolve) => {
      // Load users.
      fetch('http://vodafone.caelumlabs.com:3000/claims/4')
        .then(response => response.json())
        .then(function (data) {
          resolve(data.claims)
        })
    })
  }

  async load () {
    const that = this
    const newStatus = {
      loaded: true,
      deployed: 0,
      granted: 0,
      revoked: 0,
      campaigns: 0,
      users: 0,
      tx: [],
      claims: []
    }
    this.loadStatus()
      .then((data) => {
        newStatus.deployed = data.stats.deployed
        newStatus.granted = data.stats.granted
        newStatus.revoked = data.stats.revoked
        newStatus.users = data.stats.users
        newStatus.campaigns = 4
        newStatus.tx = data.tx
        return that.loadClaims()
      })
      .then((claims) => {
        newStatus.claims = claims
        this.setState({ status: newStatus })
      })
  }

  render () {
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12" className="mb-4">
            <CardTitle className="mb-4">Blockchain Dashboard : Stats and Transactions</CardTitle>

            <Row className="icon-cards-row mb-2">
              <Colxx xxs="6" sm="4" md="3" lg="2">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    FutureDrive Car Leasing
                    <p className="card-text font-weight-semibold mb-0">Rewards for great drivers</p>

                    <Button href="http://vodafone.caelumlabs.com:3000/data/vauto-4.json">Download users data</Button>

                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="2">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-User"/>
                    <p className="card-text font-weight-semibold mb-0">Deployed Identities</p>
                    <p className="lead text-center">{this.state.status.deployed}</p>
                  </CardBody>
                </Card>
              </Colxx>

              <Colxx xxs="6" sm="4" md="3" lg="2">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Security-Check"/>
                    <p className="card-text font-weight-semibold mb-0">Permissions Given</p>
                    <p className="lead text-center">{this.state.status.granted}</p>
                  </CardBody>
                </Card>
              </Colxx>
              <Colxx xxs="6" sm="4" md="3" lg="2">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Security-Block"/>
                    <p className="card-text font-weight-semibold mb-0">Revoked Permissions</p>
                    <p className="lead text-center">{this.state.status.revoked}</p>
                  </CardBody>
                </Card>
              </Colxx>
              <Colxx xxs="6" sm="4" md="3" lg="2">
                <Card className="mb-4 card-home">
                  <CardBody className="text-center">
                    <i className="iconsmind-Affiliate"/>
                    <p className="card-text font-weight-semibold mb-0">Active Users</p>
                    <p className="lead text-center">{this.state.status.users}</p>
                  </CardBody>
                </Card>
              </Colxx>
            </Row>
          </Colxx>
        </Row>
        <Row>
          <Colxx xxs="12" className="mb-4">
            <Row className="icon-cards-row mb-2">
              <Colxx xxs="6" sm="6" md="6" lg="6">
                <CardTitle className="mb-4">Blockchain Transactions</CardTitle>
                <ListGroup>
                  {this.state.status.loaded === true &&
                  <ListGroup>
                    {this.state.status.tx.map((item, key) =>
                      <ListGroupItem key={key}>
                        <Row>
                          <Colxx xss="4">
                            <strong>Action : </strong>{item.type}
                          </Colxx>
                          <Colxx xss="4">
                            <a href={'https://goerli.etherscan.io/tx/' + item.txHash} target="_blank"
                              rel="noopener noreferrer">{item.txHash}</a>
                          </Colxx>
                        </Row>
                      </ListGroupItem>
                    )}
                  </ListGroup>
                  }

                </ListGroup>
              </Colxx>
              <Colxx xxs="6" sm="6" md="6" lg="6">
                <CardTitle className="mb-4">Permisions Granted</CardTitle>
                {this.state.status.loaded === true &&
                <ListGroup>
                  {this.state.status.claims.map((item, key) =>
                    <ListGroupItem key={key}>
                      <Row>
                        <Colxx xss="4">
                          <strong>{item.claim_shared}</strong>
                        </Colxx>
                        <Colxx xss="6">
                          <strong>shared</strong><br/>
                          <br/>Data : {item.data_shared}
                        </Colxx>
                        <Colxx xss="2">
                          <div className="col-home">
                            <strong>Token</strong><br/>
                            {item.token}
                          </div>
                        </Colxx>
                      </Row>
                    </ListGroupItem>
                  )}
                </ListGroup>
                }
              </Colxx>
            </Row>
          </Colxx>
        </Row>
      </Fragment>
    )
  }
}
