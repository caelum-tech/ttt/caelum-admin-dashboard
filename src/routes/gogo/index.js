import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import start from './start'
import about from './about'

const GoGo = ({ match }) => (
  <Switch>
    <Redirect exact from={`${match.url}/`} to={`${match.url}/start`} />
    <Route path={`${match.url}/start`} component={start} />
    <Route path={`${match.url}/about`} component={about} />
    <Redirect to="/error" />
  </Switch>
)

export default GoGo
