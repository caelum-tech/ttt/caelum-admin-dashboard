import React, { Component, Fragment } from 'react'
import IntlMessages from 'Util/IntlMessages'
import { Row, Card, CardBody, Jumbotron } from 'reactstrap'

import { Colxx, Separator } from 'Components/CustomBootstrap'
import BreadcrumbContainer from 'Components/BreadcrumbContainer'

export default class GoGoAbout extends Component {
  render () {
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12">
            <BreadcrumbContainer
              heading={<IntlMessages id="menu.start" />}
              match={this.props.match}
            />
            <Separator className="mb-5" />
          </Colxx>
        </Row>
        <Row>
          <Colxx xxs="12" className="mb-4">
            <Card>
              <CardBody>
                <Jumbotron>
                  <h1 className="display-4">
                    <IntlMessages id="about.welcome" />
                  </h1>
                  <p className="lead">
                    <IntlMessages id="about.lead" />
                  </p>
                  <hr className="my-4" />
                  <p>
                    <IntlMessages id="about.lead-detail" />
                  </p>
                </Jumbotron>
              </CardBody>
            </Card>
          </Colxx>
        </Row>
      </Fragment>
    )
  }
}
