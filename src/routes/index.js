import React, { Component } from 'react'
import { Route, withRouter, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import TopNav from 'Containers/TopNav'
import Sidebar from 'Containers/Sidebar'

import gogo from './gogo'
import savedrive from './savedrive'
import parking from './parking'
import future from './future'
import woodhill from './woodhill'

class MainApp extends Component {
  render () {
    const { match, containerClassnames } = this.props
    return (
      <div id="app-container" className={containerClassnames}>
        <TopNav history={this.props.history} />
        <Sidebar />
        <main>
          <div className="container-fluid">
            <Switch>

              <Route path={`${match.url}/parking`} component={parking} />
              <Route path={`${match.url}/savedrive`} component={savedrive} />
              <Route path={`${match.url}/future`} component={future} />
              <Route path={`${match.url}/woodhill`} component={woodhill} />
              <Route path={`${match.url}/gogo`} component={gogo} />
              <Redirect to="/error" />
            </Switch>
          </div>
        </main>
      </div>
    )
  }
}

MainApp.propTypes = {
  containerClassnames: PropTypes.any
}

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu
  return { containerClassnames }
}

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(MainApp)
)
