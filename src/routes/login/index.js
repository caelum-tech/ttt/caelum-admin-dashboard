import React, { Component, Fragment } from 'react'
import { Row, Card, Form } from 'reactstrap'

import { Colxx } from 'Components/CustomBootstrap'

import { connect } from 'react-redux'
import PropTypes from 'prop-types'

const QRCode = require('qrcode.react')

class LoginLayout extends Component {
  componentDidMount () {
    console.log('Inside Login')
    document.body.classList.add('background')
  }

  componentWillUnmount () {
    document.body.classList.remove('background')
  }

  render () {
    return (
      <Fragment>
        <div className="fixed-background" />
        <main>
          <div className="container">
            <Row className="h-100">
              <Colxx xxs="12" md="10" className="mx-auto my-auto">
                <Card className="auth-card">
                  <div className="position-relative image-side ">
                  </div>
                  <div className="form-side">
                    <Form>
                      <div className="d-flex justify-content-between align-items-center">
                        <QRCode value={this.props.socket} size={250}/>
                      </div>
                    </Form>
                  </div>
                </Card>
              </Colxx>
            </Row>
          </div>
        </main>
      </Fragment>
    )
  }
}

LoginLayout.propTypes = {
  socket: PropTypes.string
}

const mapStateToProps = ({ auth }) => {
  const { user, socket } = auth
  return { user, socket }
}

export default connect(
  mapStateToProps,
  {}
)(LoginLayout)
