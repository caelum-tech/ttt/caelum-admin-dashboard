import {
  SET_SOCKET,
  LOGIN_USER
} from 'Constants/actionTypes'

const INIT_STATE = {
  user: null,
  socket: 'null'
}

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_SOCKET:
      return { ...state, socket: action.payload }
    case LOGIN_USER:
      return { ...state, user: action.payload }
    default:
      return { ...state }
  }
}
