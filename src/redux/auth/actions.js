import {
  SET_SOCKET,
  LOGIN_USER
} from 'Constants/actionTypes'

export const loginUser = (user) => {
  return (
    {
      type: LOGIN_USER,
      payload: user
    }
  )
}

export const setSocket = (socketId) => {
  return (
    {
      type: SET_SOCKET,
      payload: socketId
    }
  )
}
