import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, Switch } from 'react-router-dom'
import { IntlProvider } from 'react-intl'
import { NotificationContainer } from 'Components/ReactNotifications'
import { defaultStartPath } from 'Constants/defaultValues'
import { loginUser, setSocket } from '../redux/actions'

import { ProtectedRoute } from '../routes/ProtectedRoute'
import AppLocale from '../lang'

import MainRoute from 'Routes'
import error from 'Routes/error'
import login from 'Routes/login'

import 'Assets/css/vendor/bootstrap.min.css'
import 'react-perfect-scrollbar/dist/css/styles.css'
import 'Assets/css/sass/themes/gogo.light.red.scss'
import PropTypes from 'prop-types'

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      socketId: ''
    }
  }

  componentWillMount () {
    const socket = new WebSocket('ws://localhost:3000')

    socket.addEventListener('open', () => {
      socket.addEventListener('message', ({ data }) => {
        try {
          const msg = JSON.parse(data)

          switch (msg.type) {
            case 'accept':
              socket.close()
              this.props.loginUser(true)
              this.props.history.push('/app/savedrive')
              break
            case 'init':
              this.setState({ socketId: msg.id })
          }
        } catch (_) {
          console.error('bad message')
        }
      })
    })
  }

  render () {
    const { location, match, locale, user } = this.props
    const currentAppLocale = AppLocale[locale]

    if (location.pathname === '/' || location.pathname === '/app' || location.pathname === '/app/') {
      return (<Redirect to={defaultStartPath}/>)
    }

    return (
      <Fragment>
        <NotificationContainer/>
        <IntlProvider
          locale={currentAppLocale.locale}
          messages={currentAppLocale.messages}
        >
          <Fragment>
            <Switch>
              <ProtectedRoute
                path={`${match.url}app`}
                authUser={user}
                component={MainRoute}
              />

              <Route path={`/login`} component={login} socketId={this.state.socketId}/>
              <Route path={`/error`} component={error}/>
              <Redirect to="/error"/>
            </Switch>
          </Fragment>
        </IntlProvider>
      </Fragment>
    )
  }
}

App.propTypes = {
  loginUser: PropTypes.any,
  setSocket: PropTypes.any
}

const mapStateToProps = ({ settings, auth }) => {
  const { locale } = settings
  const { user } = auth
  return { locale, user }
}

export default connect(
  mapStateToProps,
  {
    loginUser,
    setSocket
  }
)(App)
