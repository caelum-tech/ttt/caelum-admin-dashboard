/* Gogo Language Texts

Table of Contents

01.General
02.User Login, Logout, Register
03.Menu
04.Error Page
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'Gogo React © 2018 All Rights Reserved.',

  /* 02.User Login, Logout, Register */
  'user.login-title': 'Login',
  'user.register': 'Register',
  'user.forgot-password': 'Forgot Password',
  'user.email': 'E-mail',
  'user.password': 'Password',
  'user.forgot-password-question': 'Forget password?',
  'user.fullname': 'Full Name',
  'user.login-button': 'LOGIN',
  'user.register-button': 'REGISTER',
  'user.reset-password-button': 'RESET',

  /* 03.Menu */
  'menu.app': 'Home',
  'menu.gogo': 'Gogo',
  'menu.start': 'Start',
  'menu.parking': 'Parking',
  'menu.second': 'Second',
  'menu.ui': 'UI',
  'menu.charts': 'Charts',
  'menu.chat': 'Chat',
  'menu.survey': 'Survey',
  'menu.todo': 'Todo',
  'menu.search': 'Search',
  'menu.about': 'About this App',

  /* 04.Error Page */
  'layouts.error-title': 'Ooops... looks like an error occurred!',
  'layouts.error-code': 'Error code',
  'layouts.go-back-home': 'GO BACK HOME',

  /* 05.Home */
  'home.welcome': 'Welcome to Node Admin',
  'home.lead': 'From here you can admin your own Blockchain node, add daps, manage users...',
  'home.lead-detail': 'Details',
  'home.login': 'Login',

  /* 06.Dashboards */
  'dashboards.transactions.chart': 'Transaction History'

}
