/* Gogo Language Texts

Table of Contents

01.General
02.Inicio de sesión de usuario, cierre de sesión, registro
03.Menú
04.Error
*/

module.exports = {
  /* 01.General */
  'general.copyright': 'Gogo React © Todos los derechos reservados.',

  /* 02.Inicio de sesión de usuario, cierre de sesión, registro */
  'user.login-title': 'Iniciar sesión',
  'user.register': 'Registro',
  'user.forgot-password': 'Se te olvidó tu contraseña',
  'user.email': 'Email',
  'user.password': 'Contraseña',
  'user.forgot-password-question': '¿Contraseña olvidada?',
  'user.fullname': 'Nombre completo',
  'user.login-button': 'INICIAR SESIÓN',
  'user.register-button': 'REGISTRO',
  'user.reset-password-button': 'REINICIAR',

  /* 03.Menú */
  'menu.app': 'Inicio',
  'menu.gogo': 'Gogo',
  'menu.start': 'Comienzo',
  'menu.second-menu': 'Segundo menú',
  'menu.ui': 'IU',
  'menu.charts': 'Gráficos',
  'menu.chat': 'Chatea',
  'menu.survey': 'Encuesta',
  'menu.todo': 'Notas',
  'menu.search': 'Búsqueda',

  /* 04.Error  */
  'layouts.error-title': 'Vaya, parece que ha ocurrido un error!',
  'layouts.error-code': 'Código de error',
  'layouts.go-back-home': 'REGRESAR A INICIO',

  /* 05.Home */
  'home.welcome': 'Bienvenido a Node Admin',
  'home.lead': 'Desde aquí podrás administrar tu propio nodo de Blockchain así como añadir Dapps y gestionar usuarios',
  'home.lead-detail': 'Detalles',
  'home.login': 'Login',

  /* 06.Dashboards */
  'dashboards.transactions.chart': 'Histórico de Transacciones'
}
