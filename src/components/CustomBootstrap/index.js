import React from 'react'
import { Col } from 'reactstrap'
import PropTypes from 'prop-types'

const Colxx = (props) => (
  <Col {...props} widths={['xxs', 'xs', 'sm', 'md', 'lg', 'xl', 'xxl']}/>
)
const Separator = (props) => <div className={`separator ${props.className}`}/>

Separator.propTypes = {
  className: PropTypes.string
}

export { Colxx, Separator }
